package mycontroller;

import mymapper.StudentMapper;
import mypo.Student;
import org.apache.ibatis.io.Resources;
import org.apache.ibatis.session.SqlSession;
import org.apache.ibatis.session.SqlSessionFactory;
import org.apache.ibatis.session.SqlSessionFactoryBuilder;

import java.io.InputStream;

public class MysqlOperation {

    public Student getStudent(int sno) throws Exception {
        String resource="SqlMapConfig.xml";

        InputStream inputStream = Resources.getResourceAsStream(resource);

        //创建会话工厂,传入mybatis的配置文件信息
        SqlSessionFactory sqlSessionFactory = new SqlSessionFactoryBuilder().build(inputStream);

        SqlSession sqlSession = sqlSessionFactory.openSession();

        //创建UserMapper的对象
        StudentMapper studentMapper = sqlSession.getMapper(StudentMapper.class);

        //调用userMapper的方法
        Student student = studentMapper.findStudentBySno(sno);

        sqlSession.close();

        return student;
    }
}
