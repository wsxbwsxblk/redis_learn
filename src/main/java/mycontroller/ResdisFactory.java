package mycontroller;

import redis.clients.jedis.JedisPool;
import redis.clients.jedis.JedisPoolConfig;

import java.net.URI;

public class ResdisFactory {
    private static JedisPool jedisPool = null;

    private ResdisFactory(){}

    public static JedisPool getInstance(){
        if(jedisPool==null){
            synchronized (ResdisFactory.class){
                if(jedisPool==null){
                    JedisPoolConfig config = new JedisPoolConfig();
                    config.setMaxTotal(64);
                    config.setMaxIdle(64);
                    config.setMinIdle(64);
                    config.setTestOnBorrow(true);
                    config.setTestOnReturn(true);
                    config.setTestWhileIdle(true);
                    config.setMaxWaitMillis(3000);
                    config.setMinEvictableIdleTimeMillis(60);
                    config.setTimeBetweenEvictionRunsMillis(30);
                    config.setBlockWhenExhausted(false);
                    URI uri = URI.create("redis://192.168.176.202:6379");

                    jedisPool = new JedisPool(config,uri,2000,2000);
                }
            }
        }

        return jedisPool;
    }
}

