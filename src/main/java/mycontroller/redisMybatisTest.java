package mycontroller;

import mypo.Student;
import org.junit.Test;

public class redisMybatisTest {

    @Test
    public void testRedisMybatis() throws Exception {

        RedisOperation redisOperation = new RedisOperation();
        String res = redisOperation.getVal("sno110");
        if(res==null) {
            MysqlOperation mysqlOperation = new MysqlOperation();
            Student student = mysqlOperation.getStudent(110);
            redisOperation.setValFor30min("sno110",student);
        }else{
            System.out.println("该学生已存在redis数据库");
        }
    }

    @Test
    public void testSetRedis(){

        Student student = new Student();
        student.setSno(112);
        student.setName("王五");
        student.setAge(18);

        RedisOperation redisOperation = new RedisOperation();
        redisOperation.setVal("sno2",student);
    }

    @Test
    public void testDeleteRedis(){

        RedisOperation redisOperation = new RedisOperation();
        redisOperation.deleteVal("sno2");
    }
}
