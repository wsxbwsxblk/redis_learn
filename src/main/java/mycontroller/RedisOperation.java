package mycontroller;

import com.alibaba.fastjson.JSONArray;
import mypo.Student;
import redis.clients.jedis.Jedis;

import java.sql.SQLOutput;
import java.util.List;

public class RedisOperation {
    public void setVal(String key, Student student){
        final Jedis resource = ResdisFactory.getInstance().getResource();
        Object obj = JSONArray.toJSON(student);
        String json = obj.toString();
        try {
            resource.set(key,json);
        }finally {
            resource.close();
        }
    }

    public void setValFor30min(String key, Student student){
        final Jedis resource = ResdisFactory.getInstance().getResource();
        Object obj = JSONArray.toJSON(student);
        String json = obj.toString();
        try {
            resource.set(key,json);
            resource.expire(key,1800);
        }finally {
            resource.close();
        }
    }

    public String getVal(String key){
        final Jedis resource = ResdisFactory.getInstance().getResource();
        try {
            String res = resource.get(key);
            return res;
        }finally {
            resource.close();
        }
    }

    public void deleteVal(String key){
        final Jedis resource = ResdisFactory.getInstance().getResource();
        try {
            resource.del(key);
        }finally {
            resource.close();
        }
    }
}
