package mymapper;

import mypo.Student;

public interface StudentMapper {
    //查询
    public Student findStudentBySno(int sno) throws Exception;

}
